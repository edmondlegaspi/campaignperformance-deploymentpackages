import base64
import boto3
import json
from elasticsearch import Elasticsearch, RequestsHttpConnection
from requests_aws4auth import AWS4Auth
from datetime import datetime
import os
import CampaignLookup.campaign_country_map as campaign_country_map

"""
  _____          __  __ _____        _____ _____ _   _
 / ____|   /\   |  \/  |  __ \ /\   |_   _/ ____| \ | |
| |       /  \  | \  / | |__) /  \    | || |  __|  \| |
| |      / /\ \ | |\/| |  ___/ /\ \   | || | |_ | . ` |
| |____ / ____ \| |  | | |  / ____ \ _| || |__| | |\  |
 \_____/_/    \_\_|  |_|_| /_/    \_\_____\_____|_| \_|

"""

aws_session = boto3.Session()
aws_cred = aws_session.get_credentials()
service = 'es'
region = 'ap-southeast-1'
awsauth = AWS4Auth(aws_cred.access_key, aws_cred.secret_key, region, service, session_token=aws_cred.token)

es = Elasticsearch(
    hosts = [{'host': os.environ['host'], 'port': 443}],
    http_auth = awsauth,
    use_ssl = True,
    verify_certs = True,
    connection_class = RequestsHttpConnection
)

def getIndexName(id):
    try:
        file_path = os.environ['LAMBDA_TASK_ROOT'] + "config.yaml"

        temp = None

        with open(file_path, 'r') as stream:
            temp = yaml.safe_load(stream)

        brand_impression_name = temp['clients'][int(id)]
    except Exception as e:
        print("Failed to get brand id")
        return None
    else:
        return brand_impression_name


def get_country_tag_if_campaign(campaign_id):
    country_tag = None

    for i in json.loads(campaign_country_map.campaign_country_map):
        if i['campaign_id'] == campaign_id:
            country_tag = i['country_id']
            break

    return country_tag

def lambda_handler(event, context):
    row_to_insert = {}

    event_id = event['event_id']

    print("Event: {}".format(event))

    row_to_insert['app_user_id'] = event['form_response']['hidden']['a']
    row_to_insert['campaign_id'] = event['form_response']['hidden']['c']
    row_to_insert['campaign_completed_date'] = event['form_response']['submitted_at']
    row_to_insert['completed'] = 1
    row_to_insert['program_type'] = 'research'
    row_to_insert['campaign_title'] = event['form_response']['definition']['title']

    row_to_insert['country_tag'] = get_country_tag_if_campaign(int(event['form_response']['hidden']['c']))

    print("Row to Insert: {}".format(row_to_insert))

    try:
        if row_to_insert['country_tag'] == 1:
            res = es.index(index=os.environ['PH_index_campaign'], id=event_id, body=row_to_insert)
            print("Successfully logged to Elasticsearch: {}.".format(res['_id']))
        elif row_to_insert['country_tag'] == 2:
            res = es.index(index=os.environ['AU_index_campaign'], id=event_id, body=row_to_insert)
            print("Successfully logged to Elasticsearch: {}.".format(res['_id']))
        else:
            print('Failed to get the country tag identifier.')
            res = es.index(index=os.environ['DEV_index_campaign'], id=event_id, body=row_to_insert)
            print("Bakup: {}.".format(res['_id']))
    except Exception as elastic_error:
        print("Elasticsearch Error {}.".format(elastic_error))
    else:
        print("No error encountered")


    return {
        "statusCode": 200,
        "body": json.dumps('Successfully logged typeform data.')
    }
