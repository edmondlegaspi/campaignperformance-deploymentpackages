def impression_parser(data_dict):
    return {
        # promo_id
        'promo_id' : data_dict['promo_id'] if 'promo_id' in data_dict.keys() else None,
        # promo_name
        'promo_name' : data_dict['promo_name'] if 'promo_name' in data_dict.keys() else None,
        # brand_id
        'brand_id' : data_dict['brand_id'] if 'brand_id' in data_dict.keys() else None,
        # brand_name
        'brand_name' : data_dict['brand_name'] if 'brand_name' in data_dict.keys() else None,
        # start_date
        'start_date' : data_dict['start_date'] if 'start_date' in data_dict.keys() else None,
        # expiration_date
        'expiration_date' : data_dict['expiration_date'] if 'expiration_date' in data_dict.keys() else None
    }
