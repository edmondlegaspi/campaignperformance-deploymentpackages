
import base64
import boto3
import json
from elasticsearch import Elasticsearch, RequestsHttpConnection
from requests_aws4auth import AWS4Auth
from datetime import datetime, timezone, timedelta
import os
from dateutil.parser import parse


"""
 _      _______      ________   _____  _____   ____  __  __  ____   _____
| |    |_   _\ \    / /  ____| |  __ \|  __ \ / __ \|  \/  |/ __ \ / ____|
| |      | |  \ \  / /| |__    | |__) | |__) | |  | | \  / | |  | | (___
| |      | |   \ \/ / |  __|   |  ___/|  _  /| |  | | |\/| | |  | |\___ \
| |____ _| |_   \  /  | |____  | |    | | \ \| |__| | |  | | |__| |____) |
|______|_____|   \/   |______| |_|    |_|  \_\\____/|_|  |_|\____/|_____/

"""

aws_session = boto3.Session()
aws_cred = aws_session.get_credentials()
service = 'es'
region = 'ap-southeast-1'
awsauth = AWS4Auth(aws_cred.access_key, aws_cred.secret_key, region, service, session_token=aws_cred.token)

es = Elasticsearch(
    hosts = [{'host': os.environ['host'], 'port': 443}],
    http_auth = awsauth,
    use_ssl = True,
    verify_certs = True,
    connection_class = RequestsHttpConnection
)

dynamodb = boto3.resource('dynamodb', region_name=region, aws_access_key_id=aws_cred.access_key, aws_secret_access_key=aws_cred.secret_key, aws_session_token=aws_cred.token)
dynamo_db_table = dynamodb.Table(os.environ['PROMO_LOOKUP_TABLE_NAME'])

json_data = ''

ph_admin_live_promos_index = 'admin-index-live-promos'
ph_admin_running_promos_index = 'admin-index-running-promos'
au_admin_live_promos_index = 'au-admin-index-live-promos'
au_admin_running_promos_index = 'au-admin-index-running-promos'
dev_admin_live_promos_index = 'dev-index-live-promos'

""" LIVE PROMOS """


def CRUD(data_dict, crud_event=None, country_tag=None):
    expiration_date = parse(parse(data_dict['expiration_date']).strftime('%Y-%m-%dT%H:%M:%SZ'))
    start_date = parse(parse(data_dict['start_date']).strftime('%Y-%m-%dT%H:%M:%SZ'))
    one_day = timedelta(days=1)
    duration = (expiration_date - start_date).days
    running_dates = start_date
    for i in range(duration + 1):
        # print(running_dates.strftime('%Y-%m-%dT%H:%M:%SZ'))
        data_dict['running_date'] = running_dates.astimezone(timezone.utc).strftime('%Y-%m-%dT%H:%M:%SZ')
        es_id = str(data_dict['promo_id']) + "-" + str(running_dates.strftime("%Y-%m-%dT%H:%M:%SZ"))

        if country_tag == 1:
            if crud_event == 'create':
                res = es.index(index=ph_admin_running_promos_index, id=es_id, body=data_dict)
                # print("CRUD Response: {}".format(res))
            else:
                res = es.delete(index=ph_admin_running_promos_index, id=es_id)
                # print("CRUD Response: {}".format(res))
        else:
            if crud_event == 'create':
                res = es.index(index=au_admin_running_promos_index, id=es_id, body=data_dict)
                # print("CRUD Response: {}".format(res))
            else:
                res = es.delete(index=au_admin_running_promos_index, id=es_id)
                # print("CRUD Response: {}".format(res))

        running_dates = running_dates + one_day

def ES_CRUD(index_name, promo_id, data_dict, country_tag):
    if es.exists(index_name, promo_id):
        # UPDATE
        old_record = es.get(index=index_name, id=promo_id)['_source']
        es.index(index=index_name, id=promo_id, body=admin_index_live_promo_row_to_insert)
        print("Old Record: {}".format(old_record))
        CRUD(old_record, crud_event='delete', country_tag=country_tag)
        CRUD(promo_id, crud_event='create', country_tag=country_tag)
        print("Successfully logged to Elasticsearch.")
    else:
        # INSERT
        es.index(index=index_name, id=promo_id, body=data_dict)
        CRUD(data_dict, crud_event='create', country_tag=country_tag)
        print("Successfully logged to Elasticsearch.")


def lambda_handler(event, context):
    message = [base64.b64decode(record['kinesis']['data']) for record in event['Records']]
    data_dict = [json.loads(decoded_message) for decoded_message in message]

    for data_dict in data_dict_array:
        print("Data Dict: {}".format(data_dict))

        ''' INSERT TO ELASTICSEARCH - admin-index-live-promos '''
        try:
            admin_index_live_promo_row_to_insert = live_promos_parser(data_dict)


            admin_index_live_promo_row_to_insert['country_tag'] = int(dynamo_db_table.get_item(Key={ 'promo_id': data_dict['promo_id'] })['Item']['country_id'])


            # Find the associated country tag to that promo by checking first if that promo_id exists in the promo_lookup inside dynamodb
            promo_key = dynamo_db_table.get_item(Key={ 'promo_id': data_dict['activity_id'] })


            if "Item" in promo_key.keys():
                admin_index_promo_row_to_insert['country_tag'] = int(promo_key['Item']['country_id'])

                print("Row to Insert: {}".format(admin_index_promo_row_to_insert))

                ES_CRUD(indexToCountryMapping[admin_index_promo_row_to_insert['country_tag']], admin_index_promo_row_to_insert['promo_id'], admin_index_promo_row_to_insert, admin_index_promo_row_to_insert['country_tag'])


            else:
                # The promo doesn't exists yet in the promo lookup.
                res = es.index(index=os.environ['DEV_index_impression'], body=admin_index_impression_row_to_insert)
                print("BAKUP: {}.".format(res['_id']))

            #
            #
            # print("Row to Insert: {}".format(admin_index_live_promo_row_to_insert))
            #
            #
            #
            # if admin_index_live_promo_row_to_insert['country_tag'] == 1:
            #     # PH
            #     if es.exists(ph_admin_live_promos_index, admin_index_live_promo_row_to_insert['promo_id']):
            #         # UPDATE
            #         print("PH UPDATE")
            #         old_record = es.get(index=ph_admin_live_promos_index, id=admin_index_live_promo_row_to_insert['promo_id'])['_source']
            #         es.index(index=ph_admin_live_promos_index, id=admin_index_live_promo_row_to_insert['promo_id'], body=admin_index_live_promo_row_to_insert)
            #         print("Old Record: {}".format(old_record))
            #         CRUD(old_record, crud_event='delete', country_tag=1)
            #         CRUD(admin_index_live_promo_row_to_insert, crud_event='create', country_tag=1)
            #         print("Successfully logged to Elasticsearch.")
            #     else:
            #         # INSERT
            #         print("PH INSERT")
            #         es.index(index=ph_admin_live_promos_index, id=admin_index_live_promo_row_to_insert['promo_id'], body=admin_index_live_promo_row_to_insert)
            #         CRUD(admin_index_live_promo_row_to_insert, crud_event='create', country_tag=1)
            #         print("Successfully logged to Elasticsearch.")
            # elif admin_index_live_promo_row_to_insert['country_tag'] == 2:
            #     # AU
            #     if es.exists(au_admin_live_promos_index, admin_index_live_promo_row_to_insert['promo_id']):
            #         # UPDATE
            #         print("AU UPDATE")
            #         old_record = es.get(au_admin_live_promos_index, admin_index_live_promo_row_to_insert['promo_id'])['_source']
            #         es.index(index=au_admin_live_promos_index, id=admin_index_live_promo_row_to_insert['promo_id'], body=admin_index_live_promo_row_to_insert)
            #         print("Old Record: {}".format(old_record))
            #         CRUD(old_record, crud_event='delete', country_tag=2)
            #         CRUD(admin_index_live_promo_row_to_insert, crud_event='create', country_tag=2)
            #         print("Successfully logged to Elasticsearch.")
            #     else:
            #         # INSERT
            #         es.index(index=au_admin_live_promos_index, id=admin_index_live_promo_row_to_insert['promo_id'], body=admin_index_live_promo_row_to_insert)
            #         CRUD(admin_index_live_promo_row_to_insert, crud_event='create', country_tag=2)
            #         print("AU INSERT")
            #         print("Successfully logged to Elasticsearch.")
            # else:
            #     print('Failed to get the country tag identifier.')
            #     es.index(index=dev_admin_live_promos_index, id=admin_index_live_promo_row_to_insert['promo_id'], body=admin_index_live_promo_row_to_insert)
        except Exception as e:
            print("Failed to upload to ES with error: {}".format(e))
        # else:
            # print("No Error Encountered.")
