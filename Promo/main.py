import base64
import boto3
import json
from elasticsearch import Elasticsearch, RequestsHttpConnection
from requests_aws4auth import AWS4Auth
from datetime import datetime
import os
from Parser.promo_parser import promo_parser
from Parser.engagement_parser import engagement_parser
from Mapping.EngagementIndexToCountryMapping import indexEngagementToCountryMapping
from Mapping.PromoIndexCountryMapping import indexPromoToCountryMapping
from Mapping.BrandMapping import brandMapping
from Slack.SlackAlert import slack_alert

"""
 _____  _____   ____  __  __  ____    _____  ______ _____  ______ __  __ _____ _______ _____ ____  _   _
|  __ \|  __ \ / __ \|  \/  |/ __ \  |  __ \|  ____|  __ \|  ____|  \/  |  __ \__   __|_   _/ __ \| \ | |
| |__) | |__) | |  | | \  / | |  | | | |__) | |__  | |  | | |__  | \  / | |__) | | |    | || |  | |  \| |
|  ___/|  _  /| |  | | |\/| | |  | | |  _  /|  __| | |  | |  __| | |\/| |  ___/  | |    | || |  | | . ` |
| |    | | \ \| |__| | |  | | |__| | | | \ \| |____| |__| | |____| |  | | |      | |   _| || |__| | |\  |
|_|    |_|  \_\\____/|_|  |_|\____/  |_|  \_\______|_____/|______|_|  |_|_|      |_|  |_____\____/|_| \_|

"""

aws_session = boto3.Session()
aws_cred = aws_session.get_credentials()
service = 'es'
region = 'ap-southeast-1'
awsauth = AWS4Auth(aws_cred.access_key, aws_cred.secret_key, region, service, session_token=aws_cred.token)

es_aws = Elasticsearch(
    hosts = [{'host': os.environ['aws_elastic_host'], 'port': 443}],
    http_auth = awsauth,
    use_ssl = True,
    verify_certs = True,
    connection_class = RequestsHttpConnection,
    timeout=60,
    max_retries=2,
    retry_on_timeout=True
)

es_ec2 = Elasticsearch(
    hosts = [{'host': os.environ['ec2_elastic_host'], 'port': 9200}],
    http_auth=(os.environ['ec2_elastic_username'], os.environ['ec2_elastic_password']),
    timeout=60,
    max_retries=2,
    retry_on_timeout=True
)

dynamodb = boto3.resource('dynamodb', region_name=region, aws_access_key_id=aws_cred.access_key, aws_secret_access_key=aws_cred.secret_key, aws_session_token=aws_cred.token)
dynamo_db_table = dynamodb.Table(os.environ['PROMO_LOOKUP_TABLE_NAME'])

dev_admin_index_promo = 'dev-admin-index-promo'

slack_channel = os.environ['slack_channel']

""" PROMO """

def get_activity_id(data_dict):
    if "activity_id" in data_dict.keys():
        return data_dict['activity_id']
    elif "promo_id" in data_dict.keys():
        return data_dict['promo_id']
    else:
        return None

def lambda_handler(event, context):
    message = [base64.b64decode(record['kinesis']['data']) for record in event['Records']]
    data_dict_array = [json.loads(decoded_message) for decoded_message in message]

    for data_dict in data_dict_array:
        print("Data Dict: {}".format(data_dict))

        admin_index_promo_row_to_insert = promo_parser(data_dict)
        admin_index_engagement_row_to_insert = engagement_parser(data_dict)

        ''' INSERT TO ELASTICSEARCH - admin-index-promo '''
        try:
            if 'country_tag' in admin_index_engagement_row_to_insert.keys() and admin_index_engagement_row_to_insert['country_tag'] in indexEngagementToCountryMapping.keys():
                print("Data Row Promo to Insert: {}".format(admin_index_promo_row_to_insert))
                print("Data Row Engagement to Insert: {}".format(admin_index_engagement_row_to_insert))
                es_aws.index(index=indexPromoToCountryMapping[admin_index_promo_row_to_insert['country_tag']], body=admin_index_promo_row_to_insert)
                es_ec2.index(index=indexEngagementToCountryMapping[admin_index_engagement_row_to_insert['country_tag']], body=admin_index_engagement_row_to_insert)
            else:
                promo_key = dynamo_db_table.get_item(Key={ 'promo_id': get_activity_id(data_dict) })

                if "Item" in promo_key.keys():
                    admin_index_promo_row_to_insert['country_tag'] = int(promo_key['Item']['country_id'])
                    admin_index_engagement_row_to_insert['country_tag'] = int(promo_key['Item']['country_id'])
                    print("Row to Promo Insert: {}".format(admin_index_promo_row_to_insert))
                    print("Row to Engagement Insert: {}".format(admin_index_engagement_row_to_insert))
                    es_aws.index(index=indexPromoToCountryMapping[admin_index_promo_row_to_insert['country_tag']], body=admin_index_promo_row_to_insert)
                    es_ec2.index(index=indexEngagementToCountryMapping[admin_index_engagement_row_to_insert['country_tag']], body=admin_index_engagement_row_to_insert)
                else:
                    # The promo doesn't exists yet in the promo lookup.
                    admin_index_promo_row_to_insert['ERROR_LOG'] = 'PROMO_DOES_NOT_EXISTS_IN_PROMO_LOOKUP'
                    admin_index_engagement_row_to_insert['ERROR_LOG'] = 'PROMO_DOES_NOT_EXISTS_IN_PROMO_LOOKUP'
                    es_aws.index(index=dev_admin_index_promo, body=admin_index_promo_row_to_insert)
                    es_ec2.index(index=dev_admin_index_promo, body=admin_index_engagement_row_to_insert)
                    slack_alert(slack_channel, "Promo Does not exists in promo lookup.", None, data_dict)

            # Redirect to proper brand id indexes
            if admin_index_engagement_row_to_insert['brand_id'] in brandMapping.keys():
                es_aws.index(index=brandMapping[admin_index_promo_row_to_insert['brand_id']], body=admin_index_promo_row_to_insert)
                es_ec2.index(index=brandMapping[admin_index_promo_row_to_insert['brand_id']], body=admin_index_engagement_row_to_insert)

        except Exception as elastic_error:
            print("Elasticsearch Error {}.".format(elastic_error))
            data_dict['ERROR_LOG'] = 'ELASITCSEARCH_ERROR'
            es_aws.index(index=dev_admin_index_promo, body=data_dict)
            es_ec2.index(index=dev_admin_index_promo, body=data_dict)
            slack_alert(slack_channel, "Promo Redemption", "Elasticsearch Error", elastic_error, data_dict)
