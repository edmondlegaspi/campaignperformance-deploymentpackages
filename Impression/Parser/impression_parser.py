
def get_country_tag(data_dict):
    if "country_id" in data_dict.keys():
        return data_dict['country_id']
    elif "country_tag" in data_dict.keys():
        return data_dict['country_tag']
    else:
        return None


def get_activity_id(data_dict):
    if "activity_id" in data_dict.keys():
        return data_dict['activity_id']
    elif "promo_id" in data_dict.keys():
        return data_dict['promo_id']
    else:
        return None

def get_user_location(data_dict):
    if 'user_location' in data_dict.keys():
        if data_dict['user_location'] == 'null,null':
            return None
        else:
            return data_dict['user_location']
    else:
        return None


def impression_parser(data_dict):
    return {
        # app_user_id
        'app_user_id' : data_dict['app_user_id'] if 'app_user_id' in data_dict.keys() else None,
        # app_user_gender
        'app_user_gender' : data_dict['app_user_gender'] if 'app_user_gender' in data_dict.keys() else None,
        # app_user_marital_status
        'app_user_marital_status' : data_dict['app_user_marital_status'] if 'app_user_marital_status' in data_dict.keys() else None,
        # age_group_description
        'age_group_description' : data_dict['age_group_description'] if 'age_group_description' in data_dict.keys() else None,
        # area_name
        'area_name' : data_dict['area_name'] if 'area_name' in data_dict.keys() else None,
        # province_name
        'province_name' : data_dict['province_name'] if 'province_name' in data_dict.keys() else None,
        # municipality_city_name
        'municipality_city_name' : data_dict['municipality_city_name'] if 'municipality_city_name' in data_dict.keys() else None,
        # sec_name
        'sec_name' : data_dict['sec_name'] if 'sec_name' in data_dict.keys() else None,
        # program_type
        'program_type' : data_dict['program_type'] if 'program_type' in data_dict.keys() else None,
        # promo_id/campaign_id
        'activity_id' : get_activity_id(data_dict),
        # campaign_name
        'campaign_name' : data_dict['campaign_name'] if 'campaign_name' in data_dict.keys() else None,
        # campaign_title
        'campaign_title' : data_dict['campaign_title'] if 'campaign_title' in data_dict.keys() else None,
        # campaign_description
        'campaign_description' : data_dict['campaign_description'] if 'campaign_description' in data_dict.keys() else None,
        # hour_of_day
        'hour_of_day' : data_dict['hour_of_day'] if 'hour_of_day' in data_dict.keys() else None,
        # day_of_week
        'day_of_week' : data_dict['day_of_week'] if 'day_of_week' in data_dict.keys() else None,
        # day_arranger
        'day_arranger' : data_dict['day_arranger'] if 'day_arranger' in data_dict.keys() else None,
        # views
        'views' : data_dict['views'] if 'views' in data_dict.keys() else None,
        # clicks
        'clicks' : data_dict['clicks'] if 'clicks' in data_dict.keys() else None,
        # impression_date
        'impression_date' : data_dict['impression_date'] if 'impression_date' in data_dict.keys() else None,
        # user_location
        'user_location' : get_user_location(data_dict),
        # is_gps_enabled
        'is_gps_enabled' : data_dict['is_gps_enabled'] if 'is_gps_enabled' in data_dict.keys() else None,
        # is_gps_mocked
        'is_gps_mocked' : data_dict['is_gps_mocked'] if 'is_gps_mocked' in data_dict.keys() else None,
        # country_tag
        'country_tag' : get_country_tag(data_dict),
        # redemption_limit
        'redemption_limit' : data_dict['redemption_limit'] if 'redemption_limit' in data_dict.keys() else None,
        # is_gps_mocked
        'promo_type_id' : data_dict['promo_type_id'] if 'promo_type_id' in data_dict.keys() else None,
        # branch_id
        'branch_id' : data_dict['branch_id'] if 'branch_id' in data_dict.keys() else None,
        # brand_id
        'brand_id' : data_dict['brand_id'] if 'brand_id' in data_dict.keys() else None,
        # brand_name
        'brand_name' : data_dict['brand_name'] if 'brand_name' in data_dict.keys() else None
    }
