import base64
import boto3
import json
from elasticsearch import Elasticsearch, RequestsHttpConnection
from requests_aws4auth import AWS4Auth
from datetime import datetime
import os
import socket
import CampaignLookup.campaign_country_map as campaign_country_map
import CampaignLookup.campaign as campaign
from Parser.impression_parser import impression_parser
from Parser.engagement_parser import engagement_parser
from Mapping.ImpressionIndexToCountryMapping import indexImpressionToCountryMapping
from Mapping.EngagementIndexToCountryMapping import indexEngagementToCountryMapping
from Mapping.BrandMapping import brandMapping
from Slack.SlackAlert import slack_alert


"""
 _____ __  __ _____  _____  ______  _____ _____ _____ ____  _   _
|_   _|  \/  |  __ \|  __ \|  ____|/ ____/ ____|_   _/ __ \| \ | |
  | | | \  / | |__) | |__) | |__  | (___| (___   | || |  | |  \| |
  | | | |\/| |  ___/|  _  /|  __|  \___ \\___ \  | || |  | | . ` |
 _| |_| |  | | |    | | \ \| |____ ____) |___) |_| || |__| | |\  |
|_____|_|  |_|_|    |_|  \_\______|_____/_____/|_____\____/|_| \_|

"""

aws_session = boto3.Session()
aws_cred = aws_session.get_credentials()
service = 'es'
region = 'ap-southeast-1'
awsauth = AWS4Auth(aws_cred.access_key, aws_cred.secret_key, region, service, session_token=aws_cred.token)

es_ec2 = Elasticsearch(
    hosts = [{'host': os.environ['ec2_elastic_host'], 'port': 9200}],
    http_auth=(os.environ['ec2_elastic_username'], os.environ['ec2_elastic_password']),
    timeout=60,
    max_retries=2,
    retry_on_timeout=True

)

es_aws = Elasticsearch(
    hosts = [{'host': os.environ['aws_elastic_host'], 'port': 443}],
    http_auth = awsauth,
    use_ssl = True,
    verify_certs = True,
    connection_class = RequestsHttpConnection,
    timeout=60,
    max_retries=2,
    retry_on_timeout=True
)

dynamodb = boto3.resource('dynamodb', region_name=region, aws_access_key_id=aws_cred.access_key, aws_secret_access_key=aws_cred.secret_key, aws_session_token=aws_cred.token)
dynamo_db_table = dynamodb.Table(os.environ['PROMO_LOOKUP_TABLE_NAME'])

dev_admin_index_impression = 'dev-admin-index-impression'

slack_channel = os.environ['slack_channel']

""" IMPRESSION """

def get_activity_id(data_dict):
    if "activity_id" in data_dict.keys():
        return data_dict['activity_id']
    elif "promo_id" in data_dict.keys():
        return data_dict['promo_id']
    else:
        return None

def lambda_handler(event, context):
    message = [base64.b64decode(record['kinesis']['data']) for record in event['Records']]
    data_dict_array = [json.loads(decoded_message) for decoded_message in message]

    for data_dict in data_dict_array:

        print("Data Dict: {}".format(data_dict))

        admin_index_impression_row_to_insert = impression_parser(data_dict)
        admin_index_engagement_row_to_insert = engagement_parser(data_dict)

        ''' INSERT TO ELASTICSEARCH - admin-index-impression '''
        try:
            if 'country_tag' in admin_index_engagement_row_to_insert.keys() and admin_index_engagement_row_to_insert['country_tag'] in indexEngagementToCountryMapping.keys():
                print("Data Row Impression to Insert: {}".format(admin_index_impression_row_to_insert))
                print("Data Row Engagement to Insert: {}".format(admin_index_engagement_row_to_insert))

                es_aws.index(index=indexImpressionToCountryMapping[admin_index_impression_row_to_insert['country_tag']], body=admin_index_impression_row_to_insert)
                es_ec2.index(index=indexEngagementToCountryMapping[admin_index_engagement_row_to_insert['country_tag']], body=admin_index_engagement_row_to_insert)
            else:
                # Check if the impression came from campaign or promo
                if 'program_type' in data_dict.keys() and data_dict['program_type'] == 'promo':
                    # Find the associated country tag to that promo by checking first if that promo_id exists in the promo_lookup inside dynamodb
                    promo_key = dynamo_db_table.get_item(Key={ 'promo_id': get_activity_id(data_dict) })

                    if "Item" in promo_key.keys():
                        admin_index_impression_row_to_insert['country_tag'] = int(promo_key['Item']['country_id'])
                        admin_index_engagement_row_to_insert['country_tag'] = int(promo_key['Item']['country_id'])
                        print("Row to Impression Insert: {}".format(admin_index_impression_row_to_insert))
                        print("Row to Engagement Insert: {}".format(admin_index_engagement_row_to_insert))
                        es_aws.index(index=indexImpressionToCountryMapping[admin_index_impression_row_to_insert['country_tag']], body=admin_index_impression_row_to_insert)
                        es_ec2.index(index=indexEngagementToCountryMapping[admin_index_engagement_row_to_insert['country_tag']], body=admin_index_engagement_row_to_insert)
                    else:
                        # The promo doesn't exists yet in the promo lookup.
                        admin_index_impression_row_to_insert['ERROR_LOG'] = 'PROMO_DOES_NOT_EXISTS_IN_PROMO_LOOKUP'
                        admin_index_engagement_row_to_insert['ERROR_LOG'] = 'PROMO_DOES_NOT_EXISTS_IN_PROMO_LOOKUP'
                        es_aws.index(index=dev_admin_index_impression, body=admin_index_impression_row_to_insert)
                        es_ec2.index(index=dev_admin_index_impression, body=admin_index_engagement_row_to_insert)
                        slack_alert(slack_channel, "Promo Does not exists in promo lookup.", None, data_dict)
                else:
                    # The campaign country tag does not exists.
                    admin_index_impression_row_to_insert['ERROR_LOG'] = 'CAMPAIGN_DOES_NOT_HAVE_COUNTRY_TAG'
                    admin_index_engagement_row_to_insert['ERROR_LOG'] = 'CAMPAIGN_DOES_NOT_HAVE_COUNTRY_TAG'
                    es_aws.index(index=dev_admin_index_impression, body=admin_index_impression_row_to_insert)
                    es_ec2.index(index=dev_admin_index_impression, body=admin_index_engagement_row_to_insert)
                    slack_alert(slack_channel, "Campaign does not have country tag.", None, data_dict)


            # Redirect to proper brand id indexes
            if admin_index_engagement_row_to_insert['brand_id'] in brandMapping.keys():
                es_aws.index(index=brandMapping[admin_index_impression_row_to_insert['brand_id']], body=admin_index_impression_row_to_insert)
                es_ec2.index(index=brandMapping[admin_index_engagement_row_to_insert['brand_id']], body=admin_index_engagement_row_to_insert)

        except Exception as elastic_error:
            print("Elasticsearch Error {}.".format(elastic_error))
            data_dict['ERROR_LOG'] = 'ELASITCSEARCH_ERROR'
            es_ec2.index(index=dev_admin_index_impression, body=data_dict)
            es_aws.index(index=dev_admin_index_impression, body=data_dict)
            slack_alert(slack_channel, "Impression", "Elasticsearch Error", elastic_error, data_dict)
