import requests

def slack_alert(slack_channel, event_log, error_log, error_message=None, promo_id=None):
    # "<@ULP625ELF>, <@UNG891DNV>"

    data_dict = """{{"channel": "#{}", "username": "CampaignPerformanceDashboard",
                "text": "{} | {} | {} | {}. <@UM4PEGD27>",
                   "icon_emoji": ":weary:"}}""".format(slack_channel, event_log, error_log, error_message, promo_id)

    res_user = requests.post(
        "https://hooks.slack.com/services/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",
        data=data_dict,
        headers={'Content-Type': 'application/json'}
    )
